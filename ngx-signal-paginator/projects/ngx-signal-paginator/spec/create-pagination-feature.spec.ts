import { TestBed, fakeAsync, tick } from "@angular/core/testing"
import { HttpErrorResponse } from "@angular/common/http";
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from '@angular/platform-browser-dynamic/testing';
import { signalStore } from "@ngrx/signals";
import { firstValueFrom, map, of, throwError } from "rxjs";
import { createPaginationFeature } from "../src/public-api";
import { TPaginatorState } from "../src/lib/types/paginator-state.type";

describe('createPaginationFeature', () => {
  beforeAll(() => {
    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());
  });

  it('should create a paginator feature with the initial state', () => {
    const debounceMs = 200;
    const retryAttempts = 3;
    const retryDelayMs = 50;

    const mockData1 = { id: 1, name: 'example-no-1' };
    const mockData2 = { id: 2, name: 'example-no-2' };

    const fetchFn = (page: number, pageSize: number) => (
      of({ data: [ mockData2 ], totalItems: 1 })
    );

    const initialState: TPaginatorState<{ id: number, name: string }> = {
      fetchFn,
      currentPageData: [ mockData1 ],
      currentPageNumber: 1,
      requestedPageNumber: 1,
      totalItems: 1,
      pageSize: 1,
      isLoading: false,
      error: undefined,
      cache: {
        1: [ mockData1 ]
      },
    }

    const Store = signalStore(
      createPaginationFeature<{ id: number, name: string }>(
        debounceMs, retryAttempts, retryDelayMs, initialState
      )
    );

    TestBed.runInInjectionContext(() => {
      const store = new Store();

      expect(store.fetchFn()).toBe((initialState.fetchFn));
      expect(store.currentPageData()).toEqual(initialState.currentPageData);
      expect(store.currentPageNumber()).toBe(initialState.currentPageNumber);
      expect(store.requestedPageNumber()).toBe(initialState.requestedPageNumber);
      expect(store.totalItems()).toBe(initialState.totalItems);
      expect(store.pageSize()).toBe(initialState.pageSize);
      expect(store.isLoading()).toBe(initialState.isLoading);
      expect(store.error()).toBe(initialState.error);
      expect(store.cache()).toEqual(initialState.cache);
    });
  });

  it('should debounce fetch requests', fakeAsync(() => {
    const debounceMs = 200;

    const mockData = { id: 1, name: 'example-no-1' };

    const fetchFn = (page: number, pageSize: number) => (
      of({ data: [ mockData ], totalItems: 1 })
    );

    const Store = signalStore(
      createPaginationFeature<{ id: number, name: string }>(debounceMs)
    );

    TestBed.runInInjectionContext(() => {
      const store = new Store();

      expect(store.currentPageData()).not.toEqual([ mockData ]);
      expect(store.requestedPageNumber()).toBeUndefined();
      expect(store.currentPageNumber()).toBeUndefined();

      store.fetchPage({ fetchFn, page: 1, pageSize: 1 });

      expect(store.requestedPageNumber()).toBe(1);
      expect(store.currentPageNumber()).toBeUndefined();
      expect(store.currentPageData()).not.toEqual([ mockData ]);

      tick(debounceMs);

      expect(store.currentPageNumber()).toBe(1);
      expect(store.currentPageData()).toEqual([ mockData ]);
      expect(store.cache()).toEqual({ 1: [ mockData ] });
    });
  }));

  it('should retry failed fetch requests and set an error to a store', fakeAsync(() => {
    const debounceMs = 200;
    const retryAttempts = 3;
    const retryDelayMs = 50;

    const httpError = new HttpErrorResponse({ status: 400, error: 'Something went wrong' });

    const fetchFn = jest.fn().mockReturnValue(
      throwError(() => httpError)
    );

    const Store = signalStore(
      createPaginationFeature<{ id: number, name: string }>(
        debounceMs, retryAttempts, retryDelayMs
      )
    );

    TestBed.runInInjectionContext(() => {
      const store = new Store();

      store.fetchPage({ fetchFn, page: 1, pageSize: 1 });
      tick(debounceMs);
      expect(store.error()).toBeUndefined();

      tick(retryDelayMs);
      expect(store.error()).toBeUndefined();
      tick(retryDelayMs);
      expect(store.error()).toBeUndefined();
      tick(retryDelayMs);

      expect(store.error()).toBe(httpError);
    });
  }));

  it('should go to the next page', fakeAsync(() => {
    const debounceMs = 200;
    const retryAttempts = 3;
    const retryDelayMs = 50;

    const mockPage1 = { data: [{ id: 1, name: 'example-no-1' }], totalItems: 2 };
    const mockPage2 = { data: [{ id: 2, name: 'example-no-2' }], totalItems: 2 };

    const fetchFn = (page: number, pageSize: number) => (
      of(page === 1 ? mockPage1 : mockPage2)
    );

    const Store = signalStore(
      createPaginationFeature<{ id: number, name: string }>(
        debounceMs, retryAttempts, retryDelayMs, {
          fetchFn,
          currentPageData: mockPage1.data,
          currentPageNumber: 1,
          requestedPageNumber: undefined,
          totalItems: mockPage1.totalItems,
          pageSize: 1,
          isLoading: false,
          error: undefined,
          cache: {
            1: mockPage1.data
          },
        }
      )
    );

    TestBed.runInInjectionContext(() => {
      const store = new Store();

      store.fetchPage({ fetchFn, page: 1, pageSize: 1 });
      expect(store.requestedPageNumber()).toBe(1);
      tick(debounceMs);
      expect(store.currentPageData()).toEqual(mockPage1.data);

      store.nextPage();
      expect(store.requestedPageNumber()).toBe(2);
      tick(debounceMs);
      expect(store.currentPageData()).toEqual(mockPage2.data);

      store.nextPage();
      expect(store.requestedPageNumber()).toBe(2);
      expect(store.currentPageData()).toEqual(mockPage2.data);
    });
  }));

  it('should go to the previous page', fakeAsync(() => {
    const debounceMs = 200;
    const retryAttempts = 3;
    const retryDelayMs = 50;

    const mockPage1 = { data: [{ id: 1, name: 'example-no-1' }], totalItems: 2 };
    const mockPage2 = { data: [{ id: 2, name: 'example-no-2' }], totalItems: 2 };

    const fetchFn = (page: number, pageSize: number) => (
      of(page === 1 ? mockPage1 : mockPage2)
    );

    const Store = signalStore(
      createPaginationFeature<{ id: number, name: string }>(
        debounceMs, retryAttempts, retryDelayMs, {
          fetchFn,
          currentPageData: mockPage2.data,
          currentPageNumber: 1,
          requestedPageNumber: undefined,
          totalItems: mockPage2.totalItems,
          pageSize: 1,
          isLoading: false,
          error: undefined,
          cache: {
            2: mockPage2.data
          },
        }
      )
    );

    TestBed.runInInjectionContext(() => {
      const store = new Store();

      store.fetchPage({ fetchFn, page: 2, pageSize: 1 });
      expect(store.requestedPageNumber()).toBe(2);
      tick(debounceMs);
      expect(store.currentPageData()).toEqual(mockPage2.data);

      store.previousPage();
      expect(store.requestedPageNumber()).toBe(1);
      tick(debounceMs);
      expect(store.currentPageData()).toEqual(mockPage1.data);

      store.previousPage();
      expect(store.requestedPageNumber()).toBe(1);
      expect(store.currentPageData()).toEqual(mockPage1.data);
    });
  }));

  it('should go to a specific page', fakeAsync(() => {
    const debounceMs = 200;
    const retryAttempts = 3;
    const retryDelayMs = 50;

    const mockPage1 = { data: [{ id: 1, name: 'example-no-1' }], totalItems: 5 };
    const mockPage5 = { data: [{ id: 5, name: 'example-no-5' }], totalItems: 5 };

    const fetchFn = (page: number, pageSize: number) => (
      of(page === 1 ? mockPage1 : mockPage5)
    );

    const Store = signalStore(
      createPaginationFeature<{ id: number, name: string }>(
        debounceMs, retryAttempts, retryDelayMs, {
          fetchFn,
          currentPageData: mockPage1.data,
          currentPageNumber: 1,
          requestedPageNumber: undefined,
          totalItems: mockPage1.totalItems,
          pageSize: 1,
          isLoading: false,
          error: undefined,
          cache: {
            1: mockPage1.data
          },
        }
      )
    );

    TestBed.runInInjectionContext(() => {
      const store = new Store();

      store.fetchPage({ fetchFn, page: 1, pageSize: 1 });
      expect(store.requestedPageNumber()).toBe(1);
      tick(debounceMs);
      expect(store.currentPageData()).toEqual(mockPage1.data);

      store.jumpToPage(5);
      expect(store.requestedPageNumber()).toBe(5);
      tick(debounceMs);
      expect(store.currentPageData()).toEqual(mockPage5.data);
    });
  }));

  it('should set a new page size', fakeAsync(() => {
    const debounceMs = 200;
    const retryAttempts = 3;
    const retryDelayMs = 50;

    const mockPage1 = { data: [{ id: 1, name: 'example-no-1' }], totalItems: 2 };
    const mockPage2 = { data: [{ id: 2, name: 'example-no-2' }], totalItems: 2 };

    const fetchFn = (page: number, pageSize: number) => (
      of(page === 1 ? mockPage1 : mockPage2)
    );

    const Store = signalStore(
      createPaginationFeature<{ id: number, name: string }>(
        debounceMs, retryAttempts, retryDelayMs, {
          fetchFn,
          currentPageData: mockPage1.data,
          currentPageNumber: 1,
          requestedPageNumber: 1,
          totalItems: mockPage1.totalItems,
          pageSize: 1,
          isLoading: false,
          error: undefined,
          cache: {
            1: mockPage1.data
          },
        }
      )
    );

    TestBed.runInInjectionContext(() => {
      const store = new Store();

      expect(store.pageSize()).toBe(1);
      expect(store.currentPageNumber()).toBe(1);
      expect(store.currentPageData()).toEqual(mockPage1.data);
      store.setPageSize(10);
      tick(debounceMs);
      expect(store.pageSize()).toBe(10);

      store.setPageSize(1);
      tick(debounceMs);
      expect(store.pageSize()).toBe(1);

      store.nextPage();
      tick(debounceMs);
      expect(store.pageSize()).toBe(1);
      expect(store.currentPageNumber()).toBe(2);
      expect(store.currentPageData()).toEqual(mockPage2.data);

      store.setPageSize(2, true);
      tick(debounceMs);
      expect(store.pageSize()).toBe(2);
      expect(store.currentPageNumber()).toBe(1);
      expect(store.currentPageData()).toEqual(mockPage1.data);
    });
  }));

  it('should restore the initial state', fakeAsync(() => {
    const debounceMs = 200;
    const retryAttempts = 3;
    const retryDelayMs = 50;

    const mockPage1 = { data: [{ id: 1, name: 'example-no-1' }], totalItems: 2 };
    const mockPage2 = { data: [{ id: 2, name: 'example-no-2' }], totalItems: 2 };

    const fetchFn = (page: number, pageSize: number) => (
      of(page === 1 ? mockPage1 : mockPage2)
    );

    const initialState: TPaginatorState<{ id: number, name: string }> = {
      fetchFn,
      currentPageData: mockPage1.data,
      currentPageNumber: 1,
      requestedPageNumber: 1,
      totalItems: mockPage1.totalItems,
      pageSize: 1,
      isLoading: false,
      error: undefined,
      cache: {
        1: mockPage1.data
      },
    }

    const Store = signalStore(
      createPaginationFeature<{ id: number, name: string }>(
        debounceMs, retryAttempts, retryDelayMs, initialState
      )
    );

    TestBed.runInInjectionContext(() => {
      const store = new Store();

      expect(store.fetchFn()).toBe((initialState.fetchFn));
      expect(store.currentPageData()).toEqual(initialState.currentPageData);
      expect(store.currentPageNumber()).toBe(initialState.currentPageNumber);
      expect(store.requestedPageNumber()).toBe(initialState.requestedPageNumber);
      expect(store.totalItems()).toBe(initialState.totalItems);
      expect(store.pageSize()).toBe(initialState.pageSize);
      expect(store.isLoading()).toBe(initialState.isLoading);
      expect(store.error()).toBe(initialState.error);
      expect(store.cache()).toEqual(initialState.cache);

      store.nextPage();
      tick(debounceMs);

      expect(store.currentPageData()).toEqual(mockPage2.data);
      expect(store.currentPageNumber()).toBe(2);
      expect(store.requestedPageNumber()).toBe(2);
      expect(store.cache()).toEqual({
        ...initialState.cache,
        2: mockPage2.data
      });

      store.restoreToInitialState();

      expect(store.currentPageData()).toEqual(initialState.currentPageData);
      expect(store.currentPageNumber()).toBe(initialState.currentPageNumber);
      expect(store.requestedPageNumber()).toBe(initialState.requestedPageNumber);
      expect(store.cache()).toEqual(initialState.cache);
    });
  }))

  it('should clear the state', fakeAsync(() => {
    const debounceMs = 200;
    const retryAttempts = 3;
    const retryDelayMs = 50;

    const mockPage1 = { data: [{ id: 1, name: 'example-no-1' }], totalItems: 2 };
    const mockPage2 = { data: [{ id: 2, name: 'example-no-2' }], totalItems: 2 };

    const fetchFn = (page: number, pageSize: number) => (
      of(page === 1 ? mockPage1 : mockPage2)
    );

    const initialState: TPaginatorState<{ id: number, name: string }> = {
      fetchFn,
      currentPageData: mockPage1.data,
      currentPageNumber: 1,
      requestedPageNumber: 1,
      totalItems: mockPage1.totalItems,
      pageSize: 1,
      isLoading: false,
      error: undefined,
      cache: {
        1: mockPage1.data
      },
    }

    const Store = signalStore(
      createPaginationFeature<{ id: number, name: string }>(
        debounceMs, retryAttempts, retryDelayMs, initialState
      )
    );

    TestBed.runInInjectionContext(() => {
      const store = new Store();

      expect(store.fetchFn()).toBe((initialState.fetchFn));
      expect(store.currentPageData()).toEqual(initialState.currentPageData);
      expect(store.currentPageNumber()).toBe(initialState.currentPageNumber);
      expect(store.requestedPageNumber()).toBe(initialState.requestedPageNumber);
      expect(store.totalItems()).toBe(initialState.totalItems);
      expect(store.pageSize()).toBe(initialState.pageSize);
      expect(store.isLoading()).toBe(initialState.isLoading);
      expect(store.error()).toBe(initialState.error);
      expect(store.cache()).toEqual(initialState.cache);

      store.nextPage();
      tick(debounceMs);

      expect(store.currentPageData()).toEqual(mockPage2.data);
      expect(store.currentPageNumber()).toBe(2);
      expect(store.requestedPageNumber()).toBe(2);
      expect(store.cache()).toEqual({
        ...initialState.cache,
        2: mockPage2.data
      });

      store.clear();

      expect(store.fetchFn()).toBeUndefined;
      expect(store.currentPageData()).toBeUndefined;
      expect(store.currentPageNumber()).toBeUndefined;
      expect(store.requestedPageNumber()).toBeUndefined;
      expect(store.totalItems()).toBeUndefined;
      expect(store.pageSize()).toBeUndefined;
      expect(store.isLoading()).toBe(false);
      expect(store.error()).toBeUndefined;
      expect(store.cache()).toEqual({});
    });
  }));

})
