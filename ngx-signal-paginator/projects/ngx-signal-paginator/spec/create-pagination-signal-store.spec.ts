import { TestBed } from "@angular/core/testing";
import { BrowserDynamicTestingModule, platformBrowserDynamicTesting } from "@angular/platform-browser-dynamic/testing";
import { of } from "rxjs";
import { createPaginationSignalStore } from "../src/public-api";
import { TPaginatorState } from "../src/lib/types/paginator-state.type";

describe('createPaginationSignalStore', () => {
  beforeAll(() => {
    TestBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting());
  });

  it('should create a signal store with a pagination feature', () => {
    const debounceMs = 200;
    const retryAttempts = 3;
    const retryDelayMs = 50;

    const mockData1 = { id: 1, name: 'example-no-1' };
    const mockData2 = { id: 2, name: 'example-no-2' };

    const fetchFn = (page: number, pageSize: number) => (
      of({ data: [ mockData2 ], totalItems: 1 })
    );

    const initialState: TPaginatorState<{ id: number, name: string }> = {
      fetchFn,
      currentPageData: [ mockData1 ],
      currentPageNumber: 1,
      requestedPageNumber: 1,
      totalItems: 1,
      pageSize: 1,
      isLoading: false,
      error: undefined,
      cache: {
        1: [ mockData1 ]
      },
    }

    const store = TestBed.inject(
      createPaginationSignalStore<{ id: number, name: string }>(
        debounceMs, retryAttempts, retryDelayMs, initialState
      )
    );

    expect(store.fetchFn()).toBe((initialState.fetchFn));
    expect(store.currentPageData()).toEqual(initialState.currentPageData);
    expect(store.currentPageNumber()).toBe(initialState.currentPageNumber);
    expect(store.requestedPageNumber()).toBe(initialState.requestedPageNumber);
    expect(store.totalItems()).toBe(initialState.totalItems);
    expect(store.pageSize()).toBe(initialState.pageSize);
    expect(store.isLoading()).toBe(initialState.isLoading);
    expect(store.error()).toBe(initialState.error);
    expect(store.cache()).toEqual(initialState.cache);
  });
})
