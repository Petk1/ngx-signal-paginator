/*
 * Public API Surface of ngx-signal-paginator
 */


export * from './lib/creating-functions/create-pagination-feature';
export * from './lib/creating-functions/create-pagination-signal-store';
