import { Observable } from "rxjs";

export type TPaginatorState<T> = {
  fetchFn: ((page: number, pageSize: number) => Observable<{ data: T[], totalItems: number }>) | undefined;
  currentPageData: T[] | undefined;
  currentPageNumber: number | undefined;
  requestedPageNumber: number | undefined;
  totalItems: number | undefined;
  pageSize: number | undefined;
  isLoading: boolean;
  error: unknown;
  cache: Record<number, T[]>;
}
