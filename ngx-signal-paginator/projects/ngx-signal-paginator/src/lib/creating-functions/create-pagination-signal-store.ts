import { signalStore } from '@ngrx/signals';
import { TPaginatorState } from '../types/paginator-state.type';
import { createPaginationFeature } from './create-pagination-feature';

/**
 * Creates a pagination signal store.
 *
 * @param {number} [debounceMs=300] - The debounce time in milliseconds for API requests.
 * @param {number} [retryAttempts=0] - The number of retry attempts for failed API requests.
 * @param {number} [retryDelayMs=0] - The delay in milliseconds between retry attempts.
 * @param {TPaginatorState<T>} [initialState] - The initial state of the paginator.
*/
export function createPaginationSignalStore<T>(
  debounceMs: number = 300,
  retryAttempts: number = 0,
  retryDelayMs: number = 0,
  initialState: TPaginatorState<T> = {
    fetchFn: undefined,
    currentPageData: undefined,
    currentPageNumber: undefined,
    requestedPageNumber: undefined,
    totalItems: undefined,
    pageSize: undefined,
    isLoading: false,
    error: undefined,
    cache: {},
  },
) {
  return signalStore(
    { providedIn: 'root' },
    createPaginationFeature(debounceMs, retryAttempts, retryDelayMs, initialState),
  )
}
