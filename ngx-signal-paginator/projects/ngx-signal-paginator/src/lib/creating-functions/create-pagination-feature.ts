import { computed } from '@angular/core';
import { signalStoreFeature, withMethods, withState, patchState, withComputed } from '@ngrx/signals';
import { rxMethod } from '@ngrx/signals/rxjs-interop';
import { Observable, catchError, debounceTime, of, retry, switchMap, tap } from 'rxjs';
import { TPaginatorState } from '../types/paginator-state.type';


/**
 * Creates a pagination signal feature.
 *
 * @param {number} [debounceMs=300] - The debounce time in milliseconds for API requests.
 * @param {number} [retryAttempts=0] - The number of retry attempts for failed API requests.
 * @param {number} [retryDelayMs=0] - The delay in milliseconds between retry attempts.
 * @param {TPaginatorState<T>} [initialState] - The initial state of the paginator.
*/
export function createPaginationFeature<T>(
  debounceMs: number = 0,
  retryAttempts: number = 0,
  retryDelayMs: number = 0,
  initialState: TPaginatorState<T> = {
    fetchFn: undefined,
    currentPageData: undefined,
    currentPageNumber: undefined,
    requestedPageNumber: undefined,
    totalItems: undefined,
    pageSize: undefined,
    isLoading: false,
    error: undefined,
    cache: {},
  },
) {
  return signalStoreFeature(
    withState<TPaginatorState<T>>({ ...initialState }),
    withComputed(({ totalItems, pageSize }) => ({
      totalPages: computed(() => {
        const _totalItems = totalItems() || 0;
        const _pageSize = pageSize() || 1;
        return Math.ceil(_totalItems / _pageSize);
      }),
    })),
    withMethods(store => {
      const fetchPage = rxMethod((params$: Observable<{ fetchFn: (page: number, pageSize: number) => Observable<{ data: T[], totalItems: number }>, page: number, pageSize: number }>) =>
        params$.pipe(
          tap(({ page }) => patchState(store, { requestedPageNumber: page })),
          debounceTime(debounceMs),
          tap(({ fetchFn }) => patchState(store, { isLoading: true, fetchFn, error: undefined })),
          switchMap(({ fetchFn, page, pageSize }) => {
            const cachedPageData = store.cache()[page];

            if (!cachedPageData) {
              return fetchFn(page, pageSize).pipe(
                retry({ count: retryAttempts, delay: retryDelayMs }),
                tap(({ data, totalItems }) => {
                  patchState(store, {
                    currentPageData: data,
                    currentPageNumber: page,
                    totalItems,
                    pageSize,
                    isLoading: false,
                    cache: { ...store.cache(), [page]: data },
                  });
                }),
                catchError(error => {
                  patchState(store, { error, isLoading: false });
                  return of(undefined);
                })
              );
            }


            patchState(store, { isLoading: false, currentPageData: cachedPageData, currentPageNumber: page });
            return of({ data: cachedPageData, totalItems: store.totalItems() });
          })
        )
      );


      const previousPage = () => {
        const requestedPageNumber = store.requestedPageNumber() || store.currentPageNumber();
        const fetchFn = store.fetchFn();
        const pageSize = store.pageSize();

        if (fetchFn && requestedPageNumber && requestedPageNumber > 1 && pageSize && pageSize >= 1) {
          fetchPage({ fetchFn, page: requestedPageNumber - 1, pageSize });
        }
      };


      const nextPage = () => {
        let requestedPageNumber = store.requestedPageNumber() || store.currentPageNumber();
        const totalPages = store.totalPages();

        if (typeof requestedPageNumber !== 'number') {
          return;
        }

        requestedPageNumber += 1;

        const pageSize = store.pageSize();
        const fetchFn = store.fetchFn();

        if (fetchFn && totalPages && requestedPageNumber <= totalPages && pageSize && pageSize >= 1) {
          fetchPage({ fetchFn, page: requestedPageNumber, pageSize });
        }
      };

      const setPageSize = (pageSize: number, backToFirstPage = false) => {
        if (pageSize === store.pageSize()) {
          return;
        }

        patchState(store, { cache: {}, pageSize });

        const currentPageNumber = store.currentPageNumber();
        const totalPages = store.totalPages();
        const fetchFn = store.fetchFn();

        if (currentPageNumber && fetchFn) {
          let page = currentPageNumber > totalPages ? totalPages : currentPageNumber;

          if (backToFirstPage) {
            page = 1;
          }

          fetchPage({ fetchFn, page, pageSize });
        }
      };


      const jumpToPage = (page: number) => {
        if (page === store.currentPageNumber()) {
          return;
        }

        const fetchFn = store.fetchFn();
        const pageSize = store.pageSize();

        if (fetchFn && pageSize) {
          fetchPage({ fetchFn, page, pageSize });
        }
      };


      const restoreToInitialState = () => {
        patchState(store, { ...initialState });
      };


      const clear = () => {
        patchState(store, {
          fetchFn: undefined,
          currentPageData: undefined,
          currentPageNumber: undefined,
          requestedPageNumber: undefined,
          totalItems: undefined,
          pageSize: undefined,
          isLoading: false,
          error: undefined,
          cache: {},
        });
      }


      return {
        fetchPage,
        previousPage,
        nextPage,
        setPageSize,
        jumpToPage,
        clear,
        restoreToInitialState,
      };
    }),

  );
}
