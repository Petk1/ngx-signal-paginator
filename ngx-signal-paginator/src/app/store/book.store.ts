import { signalStore, withComputed } from '@ngrx/signals';
import { TBook } from '../types/book.type';
import { createPaginationFeature } from 'ngx-signal-paginator';
import { computed } from '@angular/core';


export const BooksStore = signalStore(
  { providedIn: 'root' },
  createPaginationFeature<TBook>(200, 3, 200),
  withComputed((store) => ({
    totalNumberOfPagesDisplayedBooks: computed(() => {
      const currentPageData = store.currentPageData();

      if (currentPageData) {
        return currentPageData.reduce((acc, { numberOfPages }) => acc + numberOfPages, 0);
      }

      return null;
    })
  }))
);
