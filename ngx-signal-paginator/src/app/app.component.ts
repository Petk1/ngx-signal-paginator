import { Component, inject } from '@angular/core';
import { PlaygroundPaginationFeatureComponent } from './components/playground-pagination-feature/playground-pagination-feature.component';
import { PlaygroundPaginationSignalStoreComponent } from './components/playground-pagination-signal-store/playground-pagination-signal-store.component';
import { PlaygroundWorkingWithPrimengTableComponent } from './components/playground-working-with-primeng-table/playground-working-with-primeng-table.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    PlaygroundPaginationSignalStoreComponent,
    PlaygroundPaginationFeatureComponent,
    PlaygroundWorkingWithPrimengTableComponent
  ],
  template: `
    <div class='main-container'>
      <playground-pagination-signal-store />
      <playground-pagination-feature />
      <playground-working-with-primeng-table />
    </div>
  `,
  styles: `
    :host {
      display: flex;
      flex-direction: column;
      align-items: center;
      max-width: 100dvw;
    }

    .main-container {
      display: block;
      width: 800px;
      row-gap: 50px;
    }
  `
})
export class AppComponent {}
