import { Component, effect, inject } from "@angular/core";
import { JsonPipe } from "@angular/common";
import { delay, map, of } from "rxjs";
import { createPaginationSignalStore } from 'ngx-signal-paginator';
import { TBook } from "../../types/book.type";
import { books } from "../../books";

@Component({
  selector: 'playground-pagination-signal-store',
  standalone: true,
  template: `
    <h2>createPaginationSignalStore</h2>
    <p>is loading: {{ isLoadingSig() }} </p>
    <p>requested page: {{ requestedPageNumberSig() }} </p>
    <p>page: {{ currentPageNumberSig() }} </p>
    <p>page size: {{ pageSizeSig() }} </p>
    <p>currrent page data: {{ currentPageDataSig() | json }} </p>
    <p>total pages: {{ totalPagesSig() }} </p>

    <div>
      <button (click)="previousPage()">Previous page</button>
      <button (click)="nextPage()">Next page</button>
      <button (click)="changePageSize()">Change page size</button>
      <button (click)="jumpToPage()">Jump to page</button>
    </div>
  `,
  imports: [
    JsonPipe,
  ],
})
export class PlaygroundPaginationSignalStoreComponent {
  protected store = inject(createPaginationSignalStore<TBook>(200, 3, 200, {
    fetchFn: this.fetchMethod.bind(this),
    currentPageData: books.slice(0, 1),
    currentPageNumber: 1,
    requestedPageNumber: undefined,
    totalItems: books.length,
    pageSize: 1,
    isLoading: false,
    error: undefined,
    cache: {
      1: books.slice(0, 1)
    },
  }));
  protected isLoadingSig = this.store.isLoading;
  protected requestedPageNumberSig = this.store.requestedPageNumber;
  protected currentPageNumberSig = this.store.currentPageNumber;
  protected pageSizeSig = this.store.pageSize;
  protected currentPageDataSig = this.store.currentPageData;
  protected totalPagesSig = this.store.totalPages;
  protected pageSize = 1;

  constructor() {
    effect(() => {
      if (this.store.error()) {
        console.error("[PlaygroundPaginationSignalStoreComponent] Error occured: ", this.store.error());
      }
    });
  }

  fetchMethod(page: number, pageSize: number) {
    return of(books.slice(0 + (page - 1) * pageSize, page * pageSize)).pipe(
      delay(300),
      map(data => ({
        data,
        totalItems: books.length
      })),
    );
  }

  previousPage() {
    this.store.previousPage();
  }

  nextPage() {
    this.store.nextPage();
  }

  changePageSize() {
    this.store.setPageSize(10, true);
  }

  jumpToPage() {
    this.store.jumpToPage(5);
  }

}
