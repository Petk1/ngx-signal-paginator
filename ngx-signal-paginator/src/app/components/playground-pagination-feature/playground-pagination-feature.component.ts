import { Component, effect, inject } from "@angular/core";
import { JsonPipe } from "@angular/common";
import { delay, map, of, throwError } from "rxjs";
import { BooksStore } from "../../store/book.store";
import { books } from "../../books";
import { HttpErrorResponse } from "@angular/common/http";

@Component({
  selector: 'playground-pagination-feature',
  standalone: true,
  template: `
    <h2>createPaginationFeature</h2>
    <p>is loading: {{ isLoadingSig() }} </p>
    <p>requested page: {{ requestedPageNumberSig() }} </p>
    <p>page: {{ currentPageNumberSig() }} </p>
    <p>page size: {{ pageSizeSig() }} </p>
    <p>currrent page data: {{ currentPageDataSig() | json }} </p>
    <p>error: {{ errorSig() | json }} </p>
    <p>total number of pages currently displayed books: {{ totalNumberOfPagesDisplayedBooksSig() | json }} </p>

    <div>
      <button (click)="previousPage()">Previous page</button>
      <button (click)="nextPage()">Next page</button>
      <button (click)="changePageSize()">Change page size</button>
      <button (click)="jumpToPage()">Jump to page</button>
    </div>
  `,
  imports: [
    JsonPipe,
  ],
})
export class PlaygroundPaginationFeatureComponent {
  protected store = inject(BooksStore);
  protected isLoadingSig = this.store.isLoading;
  protected requestedPageNumberSig = this.store.requestedPageNumber;
  protected currentPageNumberSig = this.store.currentPageNumber;
  protected pageSizeSig = this.store.pageSize;
  protected currentPageDataSig = this.store.currentPageData;
  protected errorSig = this.store.error;
  protected totalNumberOfPagesDisplayedBooksSig = this.store.totalNumberOfPagesDisplayedBooks;

  constructor() {
    this.store.fetchPage({ fetchFn: this.fetchMethod.bind(this), page: 1, pageSize: 1 });

    effect(() => {
      if (this.store.error()) {
        console.error("[PlaygroundPaginationFeatureComponent] Error occured: ", this.store.error());
      }
    });
  }

  fetchMethod(page: number, pageSize: number) {
    if (page === 3) {
      return throwError(() => new HttpErrorResponse({
        error: `Keep calm, don't panic, this is just an example error`,
        status: 400
      }));
    }

    return of(books.slice(0 + (page - 1) * pageSize, page * pageSize)).pipe(
      delay(300),
      map(data => ({
        data,
        totalItems: books.length
      })),
    );
  }

  previousPage() {
    this.store.previousPage();
  }

  nextPage() {
    this.store.nextPage();
  }

  changePageSize() {
    this.store.setPageSize(10);
  }

  jumpToPage() {
    this.store.jumpToPage(5);
  }

}
