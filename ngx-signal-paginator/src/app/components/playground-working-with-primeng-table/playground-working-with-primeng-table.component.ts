import { Component, inject } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { TableLazyLoadEvent, TableModule } from "primeng/table";
import { delay, map, of, tap } from "rxjs";
import { ButtonModule } from "primeng/button";
import { InputNumberModule } from "primeng/inputnumber";
import { createPaginationSignalStore } from "ngx-signal-paginator";
import { TBook } from "../../types/book.type";
import { books } from "../../books";


@Component({
  selector: 'playground-working-with-primeng-table',
  standalone: true,
  template: `
    @if(totalPagesSig(); as totalPages) {
      <p-table
          lazy
          paginator
          showJumpToPageDropdown
          [value]="currentPageDataSig()!"
          [rows]="pageSizeSig()"
          [totalRecords]="totalItemsSig()!"
          (onLazyLoad)="loadData($event)"
          [loading]="isLoadingSig()"
          [rowsPerPageOptions]="[5, 10, 15]"
        >
        <ng-template pTemplate="header">
          <tr>
            <th>Author</th>
            <th>Title</th>
          </tr>
        </ng-template>
        <ng-template pTemplate="body" let-item>
          <tr>
            <td>{{ item.author }}</td>
            <td>{{ item.title }}</td>
          </tr>
        </ng-template>
      </p-table>
    }
  `,
  imports: [
    TableModule,
    ButtonModule,
    InputNumberModule,
    FormsModule,
  ]
})
export class PlaygroundWorkingWithPrimengTableComponent {
  protected store = inject(createPaginationSignalStore<TBook>());
  protected isLoadingSig = this.store.isLoading;
  protected totalPagesSig = this.store.totalPages;
  protected requestedPageNumberSig = this.store.requestedPageNumber;
  protected currentPageNumberSig = this.store.currentPageNumber;
  protected pageSizeSig = this.store.pageSize;
  protected currentPageDataSig = this.store.currentPageData;
  protected totalItemsSig = this.store.totalItems;

  constructor() {
    this.store.fetchPage({ fetchFn: this.fetchMethod.bind(this), page: 1, pageSize: 5 });
  }

  private fetchMethod(page: number, pageSize: number) {
    return of(books.slice(0 + (page - 1) * pageSize, page * pageSize))
      .pipe(
        delay(300),
        map(data => ({
          data,
          totalItems: books.length
        })),
      );
  }

  protected loadData(event: TableLazyLoadEvent): void {
    const { first, rows } = event;
    if (typeof first !== 'number' || typeof rows !== 'number') return;

    const page = first / rows + 1;
    if (!page) return;

    if (rows !== this.pageSizeSig()) {
      this.store.setPageSize(rows);
      return;
    }

    this.store.fetchPage({ fetchFn: this.fetchMethod.bind(this), page, pageSize: rows });
  }
}
