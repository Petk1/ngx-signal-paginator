export type TBook = {
  title: string;
  author: string;
  numberOfPages: number;
}
