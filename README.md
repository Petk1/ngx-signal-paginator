# [NgxSignalPaginator](./ngx-signal-paginator/projects/ngx-signal-paginator/)

NgxSignalPaginator is an Angular library that provides a signal-based store for handling pagination. It integrates seamlessly with `@ngrx/signals` and supports features such as lazy loading, caching, error handling, and retry mechanisms.

For more information about the Signal Store, refer to the official [NgRx Documentation](https://ngrx.io/guide/signals).

## Installation

To install the library, run the following command:

```
npm install ngx-signal-paginator
```

## Usage

### createPaginationSignalStore

#### Import

```
import { createPaginationSignalStore } from "ngx-signal-paginator";
```

#### Create the Pagination Store

Use the `createPaginationSignalStore` function to create a pagination store for your data:

```
store = inject(createPaginationSignalStore<YourDataType>(debounceMs, retryAttempts, retryDelayMs, initialState));
```

- `debounceMs` - Configurable debounce time for API requests.
- `retryAttempts` - Configurable number of retry attempts for failed API requests.
- `retryDelayMs` - Configurable delay between retry attempts for failed API requests.
- `initialState` - The initial state of the paginator signal store.



#### Pass `fetchFn`, `page` and `pageSize` to the `fetchPage` method

```
this.store.fetchPage({ fetchFn: this.fetchMethod.bind(this), page: PAGE_NUMBER, pageSize: PAGE_SIZE });
```

#### Fetch the next page

```
this.store.nextPage();
```


#### Fetch the previous page

```
this.store.previousPage();
```

#### Jump to a specific page

```
this.store.jumpToPage(PAGE_NUMBER);
```

#### Caching

Caches fetched pages to reduce API calls.
You can also get cached data using:

```
this.store.cache()
```

#### Change page size

```
this.store.setPageSize(DIFFERENT_PAGE_SIZE);
```

#### Error handling

Graceful error handling and state updates. The last error is stored in the store. You can get the error using:

```
this.store.error();
```

#### Reset state

```
this.store.reset();
```


### createPaginationFeature

You can also get the Signal Paginator Store as Custom Store Feature (created using the `signalStoreFeature` function. For more information: [NgRx Documentation](https://ngrx.io/guide/signals/signal-store/custom-store-features)).

#### Import

```
import { createPaginationFeature } from 'ngx-signal-paginator';
```

#### Use the createPaginationFeature within your signal store

```
export const YourStore = signalStore(
  createPaginationFeature<YourDataType>(debounceMs, retryAttempts, retryDelayMs, initialState),
  // other Signal Store Features 
)
```

## License

This project is licensed under the MIT License.
